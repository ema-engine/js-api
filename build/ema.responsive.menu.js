/*! EMA jQuery responsive menu plugin | MIT License | v.0.2.2a */

/**
 * jQuery responsive menu plugin
 * @version 0.2.2a
 *
 * This plugin does not use any css, so you can style it as you wish.
 *
 *
 * Initialization:
 * var inst = $(".resp-menu").emaRespMenu( options );
 *
 * Getting instance:
 * var inst = $.fn.emaRespMenu.getInstance( $(".responsive-menu") );
 *
 * Methods:
 * inst.open() - Opens responsive menu
 * inst.close() - Close responsive menu
 *
 * Options:
 * {
 *     
 *      iconActiveStateSrc: null, - Path to image which will be placed when menu is Opened. You can use a data-active-src instead.
 *      iconInactiveStateSrc: null, - Path to image which will be placed when menu is Closed. You can use a data-inactive-src instead.
 *      iconStickyStateSrc: null, - Path to image which will be placed when page is scrolled. You can use a data-sticky-src instead.
 *      NOTE: Data atrinutes override JSoptions
 *
 *      maxWidth: 900, - Max width when menu should be available.
 *
 *      animationDuration: 500,
 *      animationType: "left", - "left", "right" and "top" are sported
 *      easingFunction: "linear",
 *
 *      backButtonText: "Back", - Label for back button (in case of multi level menu)
 *      closeOnOutsideClick: true, - Close menu if user clicked outside the responsive menu wrapper
 *
 *      onScreenOffset: 0, - Sets a target offset in px
 *
 *      data: [], - data array which will be showed in menu. Can be array of objects, string, or jQuery object. See Data formats.
 *
 *      Callbacks:
 *      onOpen: function(){},
 *      onClose: function(){}
 * }
 *
 *
 * *************** Data formats *********************
 * You can pass data options as:
 *
 * 1. Array of objects: {
 *      id: "any string",
 *      name: "name string",
 *      class: "classes you need to add to the element",
 *      link: "a href attr",
 *      target: "<a> target",
 *      children: [] of children
 *      }
 *
 *      
 * 2. Or you can pass a selector(string) or jQuery object as an argument which points to menu in format:
 *
 * <ul>
 *     <li>
 *         <a href='#'>Some link</a>
 *         <ul>
 *             <li><a href='#'>Sub link</a></li>
 *         </ul>
 *     </li>
 *     <li>
 *         <a href='#'>Another link</a>
 *     </li>
 * </ul>
 *
 * In such case data array will be generated automatically.
 *
 *
 * *************** Generated HTML *********************
 * 
 * This plugin generate HTML instead of using an original menu.
 * <ul>
 *      <li><a href="link" target="target" class="class"><span>name</span></a></li>    
 * </ul>
 * 
 * If the node have children - class 'has-children' will be added to <a> tag
 * 
 * Back button have class 'back-bnt'
 * 
 * Add class 'emaResp-page' to a page container
 * 
 */


(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        factory(require('jquery'));
    } else {
        factory(root.jQuery);
    }
})(window, function ($) {
    'use strict';
    $.fn.emaRespMenu = function( options ){
        if(this.length === 0){
            throw new Error("No wrapper element selected");
        }

        var useJsAnimation = true;
        if ('Modernizr' in window && Modernizr.csstransitions) {
            useJsAnimation = false;
        }

        var $self = this;

        this.children().remove();
        this.addClass('default');

        this.emaRespMenu['settings'] = $.extend(true, {}, $.fn.emaRespMenu.defaults, options);
        this.emaRespMenu['_private'] = {};

        this.emaRespMenu._private.isClosing = false;
        this.emaRespMenu._private.isOpened = false;
        this.emaRespMenu._private.isOpening = false;

        var settings = this.emaRespMenu['settings'];


        this.append('<div class="emaRespIcon--wrapper"><img width="'+settings.iconWidth+'" height="'+settings.iconHeight+'"></div>');
        this.emaRespMenu._private.$activateIconWrapper = $(this.find('.emaRespIcon--wrapper'));
        this.emaRespMenu._private.$activateIcon = $(this.find('.emaRespIcon--wrapper>img'));

        this.emaRespMenu._private.$activateIcon.click(function(e){
            e.preventDefault();
            e.stopPropagation();
            this.emaRespMenu.open();
            /*
                        if(this.emaRespMenu._private.isClosing === false && this.emaRespMenu._private.isOpening === false){
                            if(isOpened.call(this)){
                                this.emaRespMenu.close();
                            } else {

                            }
                        }
            */
        }.bind(this));

        var dataKey;
        var settingsKey;
        for (var i = 0; i <= 2; i++) {
            switch (i){
                case 0:
                    dataKey = 'inactive-src';
                    settingsKey = 'iconInactiveStateSrc';
                    break;
                case 1:
                    dataKey = 'active-src';
                    settingsKey = 'iconActiveStateSrc';
                    break;
                case 2:
                    dataKey = 'sticky-src';
                    settingsKey = 'iconStickyStateSrc';
                    break;
            }

            var dataValue = this.data(dataKey);

            if (typeof dataValue === 'string') {
                this.emaRespMenu.settings[settingsKey] = dataValue;
            }

            if(typeof this.emaRespMenu.settings[settingsKey] === "string"){
                this.emaRespMenu._private[settingsKey] = this.emaRespMenu.settings[settingsKey];
            }

        }

        if("iconInactiveStateSrc" in this.emaRespMenu._private){
            this.emaRespMenu._private.$activateIcon.attr('src', this.emaRespMenu._private.iconInactiveStateSrc);
        }



        function toggleIcon(){
            if("iconActiveStateSrc" in this.emaRespMenu._private){
                if(isOpened.call(this)){
                    var path = this.emaRespMenu._private.iconActiveStateSrc;
                } else {
                    var path = this.emaRespMenu._private.iconInactiveStateSrc;
                }
                this.emaRespMenu._private.$activateIcon.attr("src", path);
            }
        }

        function toggleStickyIcon(){
            if("iconStickyStateSrc" in $self.emaRespMenu._private){
                $self.emaRespMenu._private.$activateIcon.css('opacity', 0);
                var path = $self.emaRespMenu._private.iconInactiveStateSrc;
                if ($self.hasClass('sticky')) {
                    path = $self.emaRespMenu._private.iconStickyStateSrc;
                }
                $self.emaRespMenu._private.$activateIcon.attr("src", path);
                $self.emaRespMenu._private.$activateIcon.animate({'opacity': 1}, 200, 'linear');



            }
        }

        function isMatchMedia(){
            return window.matchMedia("(max-width: " + this.emaRespMenu.settings.maxWidth+"px)").matches
        }

        function isOpened(){
            return (this.emaRespMenu._private.$container);
        }

        function appendUniqueIndexses(data){
            var i = 0;
            function append(children){
                var currentArray = [];
                $.each(children, function(index, item){
                    i++;
                    var currentElement = item;
                    item.uniqueId = i;

                    if("children" in item){
                        currentElement['children'] = append(item.children);
                    }

                    currentArray.push(currentElement);

                });
                return currentArray;
            }
            return append(data);
        }

        function findItemByUniqueId(uniqueId){

            var currentParent = "root";
            function search(data){

                for(var i = 0; i < data.length; i++){
                    var currentObj = data[i];
                    if("uniqueId" in currentObj && currentObj.uniqueId == uniqueId){
                        return currentObj;
                    }
                    if("children" in currentObj){
                        var previousParent = currentParent;
                        currentParent = currentObj.uniqueId;
                        var returned = search(currentObj.children);
                        if(typeof returned == "object"){
                            return returned;
                        } else {
                            currentParent = previousParent;
                        }
                    }
                }

            }
            var searchResults = search(this.emaRespMenu.settings.data);
            if(typeof searchResults == "object"){
                searchResults['parentId'] = currentParent;
            }
            return searchResults;
        }


        function hideOnResize(){

            if (!isMatchMedia.call(this)) {
                if (isOpened.call(this)) {
                    this.emaRespMenu.close();
                }

            }
        }

        function generateMenuHost(){
            var settings = this.emaRespMenu.settings;
            var html = '<div class="emaResp-back"></div>' +
                '<div class="emaResp-host" style="display: none;">';

            if (settings.closeButtonInHeader) {
                html += '<div class="emaResp-header"><div><button class="emaResp-close--btn '+ settings.additionalCloseBtnClass+'"></button></div></div>';
                html += '<div class="emaResp-container"></div>';
            } else {
                html += '<div class="emaResp-container"></div>';
                html += '<div class="emaResp-footer"><div><button class="emaResp-close--btn '+settings.additionalCloseBtnClass+'"></button></div></div>';
            }
            html += '</div>';

            return html;
        }

        function generateHtml(data, previousId){
            var settings = this.emaRespMenu.settings;
            var html = "<ul>";

            if(previousId !== undefined){
                html += "<li><a href='#back' class='back-bnt' data-unique-id='"+previousId
                    +"'><span>" + settings.backButtonText
                    +"</span></a></li>"
            }

            $.each(data, function(index, item){
                var target = "";
                if("target" in item){
                    target = " target='"+item.target+"' ";
                }

                var appendedClasses = settings.additionalItemClass + ' ';

                if("class" in item){
                    appendedClasses += item['class'];
                }



                if("children" in item){
                    appendedClasses += " has-children ";
                }
                if(! ("link" in item)){
                    item.link = "#";
                }



                html += "<li><a href='"+item['link']+"' class='"+appendedClasses+"' "+target+"" +
                    "data-unique-id='"+item['uniqueId']+"'><span>"+item['name']+"</span></a></li>"
            });
            html += "</ul>";
            return html;
        }

        function appendItemActions(){
            this.emaRespMenu._private.$container.find("a").each(function(index, elem){
                $(elem).click(function(e){
                    if ($(e.target).is('.has-children, .back-bnt')) {
                        e.preventDefault();
                        this.emaRespMenu.swithOnTo($(e.currentTarget).data("unique-id"));
                    } else {
                        this.emaRespMenu.close();
                    }
                }.bind(this));
            }.bind(this));
        }





        function generateData( selector ){
            if(typeof selector === "string"){
                var $element = $(selector);
            } else {
                var $element = selector;
            }

            function parseHtml(node) {
                var currentNode = [];
                node.find("li").each(function () {
                    var $this = $(this);
                    var current_object = {};

                    current_object['name'] = $this.children("a").text();
                    current_object['link'] = $this.children("a").attr("href");
                    var target = $this.find("a").attr("target");
                    var classes = $this.find("a").attr("class");

                    if(typeof target === "string"){
                        current_object['target'] = target;
                    }

                    if(typeof classes === "string"){
                        current_object['class'] = classes;
                    }

                    var $childrenNode = $this.children("ul");

                    if($childrenNode.length > 0){
                        var children = parseHtml($childrenNode);
                        if(children.length > 0){
                            current_object['children'] = children;
                        }
                    }

                    currentNode.push(current_object);

                });
                return currentNode;
            }

            return parseHtml($element);
        }

        function appendHtmlMenu( data ) {
            var workingObj = this.emaRespMenu._private;
            var settings =this.emaRespMenu.settings;


            if (!workingObj.$container) {


                $('body').css('overflow', 'hidden').prepend(generateMenuHost.call(this));


                workingObj.$back = $('.emaResp-back');
                workingObj.$container = $('.emaResp-container');
                workingObj.$host = $('.emaResp-host');

                $('.emaResp-page').addClass('emaResp-open');


                // workingObj.$host.css(settings.animationType, '-5000px').show();

                var offset =  workingObj.$host.outerWidth();
                if (settings.animationType === 'top') {
                    offset =  workingObj.$host.outerHeight();
                }

                offset = '-'+offset+'px';

                workingObj.$host.css(settings.animationType, offset);

                workingObj.$host.show();

                if (useJsAnimation) {

                    if (!$.isEmptyObject(settings.backJsAnimationOpened)) {
                        workingObj.$back.animate(settings.backJsAnimationOpened,
                            settings, settings.easingFunction);
                    }


                    var animationTarget = {};
                    animationTarget[settings.animationType] = '0';
                    workingObj.$host.animate(animationTarget,
                        settings.animationDuration, settings.easingFunction);
                } else {
                    workingObj.$host.addClass('animation-' + settings.animationType).addClass('open');
                }




                $('.emaResp-close--btn').click(function () {
                    this.emaRespMenu.close();
                }.bind(this));

            }


            workingObj.$container.children().remove();
            workingObj.$container.append(data);
        }

        function removeHtmlMenu() {
            var self = this;

            var settings = self.emaRespMenu.settings;
            var workingObj = self.emaRespMenu._private;

            var dfr = $.Deferred(function () {
                self.emaRespMenu._private.isClosing = true;

                var dfrArr = [$.Deferred(), $.Deferred()];

                if (useJsAnimation) {
                    if (!$.isEmptyObject(settings.backJsAnimationClosed)) {
                        workingObj.$back.animate(
                            settings.backJsAnimationClosed,
                            settings.animationDuration, settings.easingFunction, function () {
                                dfrArr[0].resolve();
                            });
                    } else {
                        dfrArr[0].resolve();
                    }


                    var offset = workingObj.$host.outerWidth();

                    if (settings.animationType === 'top') {
                        offset = workingObj.$host.outerHeight();
                    }

                    var animationTarget = {};
                    animationTarget[settings.animationType] = offset;

                    workingObj.$host.animate(
                        animationTarget,
                        settings.animationDuration, settings.easingFunction, function () {
                            dfrArr[1].resolve();
                        });

                } else {
                    workingObj.$host.removeClass('open');
                    setTimeout(function () {
                        dfrArr[0].resolve();
                        dfrArr[1].resolve();
                    }, settings.animationDuration)
                }








                $.when.apply($, dfrArr).done(function(){
                    dfr.resolve();
                });

            }).done(function () {
                $('.emaResp-page').removeClass('emaResp-open');
                self.emaRespMenu._private.$back.remove();
                self.emaRespMenu._private.$host.remove();
                self.emaRespMenu._private.$container = null;
                self.emaRespMenu._private.$back = null;
                self.emaRespMenu._private.$host = null;
                $('body').css('overflow', '')

            });

            return dfr;
        }

        $(window).resize(function(){
            hideOnResize.call(this);
        }.bind(this));

        $(window).scroll(function(){
            var $inst = $self;

            if ( $(this).scrollTop() > $self.emaRespMenu.settings.stickyOffset && $inst.hasClass("default") ){

                $inst.removeClass("default").addClass("sticky");
                toggleStickyIcon();

            } else if($(this).scrollTop() <= $self.emaRespMenu.settings.stickyOffset && $inst.hasClass("sticky")) {

                $inst.removeClass("sticky").addClass("default");
                toggleStickyIcon();
            }
        });


        this.emaRespMenu.open = function(){
            if(isMatchMedia.call(this)){
                this.emaRespMenu._private.isOpening = true;

                this.emaRespMenu.settings.onOpen.call();
                this.emaRespMenu.swithOnTo();
                this.emaRespMenu._private.isOpening = false;
                this.emaRespMenu._private.isOpened = true;
                toggleIcon.call(this);
                this.emaRespMenu.settings.onOpened.call(this, this.emaRespMenu._private.$host);
            }
        }.bind(this);

        this.emaRespMenu.close = function(){


            this.emaRespMenu.settings.onClose.call();
            removeHtmlMenu.call(this);
            toggleIcon.call(this);
            this.emaRespMenu._private.isClosing = false;
            this.emaRespMenu._private.isOpened = false;


        }.bind(this);

        this.emaRespMenu.swithOnTo = function(uniqueId){

            if(isNaN(parseInt(uniqueId))){
                appendHtmlMenu.call(this, generateHtml.call(this, this.emaRespMenu.settings.data));
            } else {
                var node = findItemByUniqueId.call(this, uniqueId);
                if(typeof node == "object" && "children" in node){
                    appendHtmlMenu.call(this, generateHtml.call(this, node.children, node.parentId));
                }
            }
            appendItemActions.call(this);
        }.bind(this);

        if(typeof this.emaRespMenu.settings.data === "string" ||
            (typeof this.emaRespMenu.settings.data === "object" && this.emaRespMenu.settings.data instanceof jQuery)
        ){
            this.emaRespMenu.settings.data = generateData(this.emaRespMenu.settings.data);
        }


        this.emaRespMenu.settings.data = appendUniqueIndexses(this.emaRespMenu.settings.data);


        if(this.emaRespMenu.settings.closeOnOutsideClick){
            /*$(document).mouseup(function(e){
                if (isMatchMedia.call(this)) {
                    if(isOpened.call(this)) {
                        if($(e.target).parents(this.selector).length == 0){
                            this.emaRespMenu.close();
                        }
                    }
                }
            }.bind(this));*/
        }

        hideOnResize.call(this);
        this.data("ema-resp-menu", this.emaRespMenu);
        return this.emaRespMenu;
    };

    $.fn.emaRespMenu.getInstance = function ( node ) {
        if(!(node instanceof jQuery)){
            node = $(node);
        }
        return node.data("ema-resp-menu");
    };

    $.fn.emaRespMenu.defaults = {
        /**
         * Selector for icon that opens the menu
         * @type {string}
         */
        // iconSelector: "#navabar-resp-icon",

        /*Icons*/
        iconActiveStateSrc: null,
        iconInactiveStateSrc: null,
        iconStickyStateSrc: null,
        stickyOffset: 53,
        iconWidth: 47,
        iconHeight: 45,

        maxWidth: 900,

        /* Animation */
        animationType: "left", /* "left" and "top", "right" are sported */
        easingFunction: "linear",
        animationDuration: 700,
        backJsAnimationOpened: {},
        backJsAnimationClosed: {},



        backButtonText: "Back",
        additionalCloseBtnClass: '',
        additionalItemClass: '',
        closeOnOutsideClick: true,
        onScreenOffset: "0",
        closeButtonInHeader: true,

        data: [],

        onOpen: function(){},
        onOpened: function () {},
        onClose: function(){}
    };

});



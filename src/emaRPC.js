/**
 * EMA engine RPC library
 * @author Illia Sazhko <babai.linux@gmail.com>
 * @version 0.3.0a
 * @licence MIT
 * @module emaRPC
 * @requires jquery
 */
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        module.exports = factory(require('jquery'));
    } else {
        root.EMA = factory(root.jQuery);
    }
})(window, function ($) {
    'use strict';


    $(window).on("EMA.ready");
    $(window).on("EMA.failure");
    $(window).on("EMA.maintenance");
    $(window).on("EMA.login");
    $(window).on("EMA.logout");
    $(window).on("EMA.providerUpdated");

    /**
     * @alias module:emaRPC
     */
    var EMA = {};


    Object.defineProperty(EMA, '_META_URL', {'value': '/api/meta'});
    Object.defineProperty(EMA, '_ACCESS_REQUEST_URL', {'value': '/api/meta/access_override'});
    Object.defineProperty(EMA, '_RPC_BASEPATH', {'value': '/api/rpc'});
    Object.defineProperty(EMA, '_UPLOAD_BATHBATH', {'value': '/api/upload'});
    Object.defineProperty(EMA, '_LOGIN_URL', {'value': '/login', 'writable': true});
    Object.defineProperty(EMA, '_LOGOUT_URL', {'value': '/logout', 'writable': true});
    Object.defineProperty(EMA, '_SESSION_URL', {'value': '/login/session', 'writable': true});
    Object.defineProperty(EMA, '_CAPTCHA_URL', {'value': '/login/captcha', 'writable': true});
    Object.defineProperty(EMA, '_SESSION_INTERVAL', {'value': 3});


    /**
     * Base url for all requests
     * @type {string}
     */
    EMA.baseUrl = '';

    /**
     * Debug flag. Set true to printout messages.
     * @type {boolean}
     */
    EMA.debug = true;

    /**
     * Is session check by interval should be used
     * @type {boolean}
     */
    EMA.sessionChecker  = true;

    /**
     * Initiate model structure
     * @return {jQuery.Deferred}
     * @fires 'EMA.ready'
     * @fires 'EMA.failure'
     */
    EMA.init = function () {
        var $dfr = $.Deferred();
        EMA.GET(EMA._META_URL).then(function (data) {
            EMA.system._bindModels(data.methods);
            EMA.system._bindProviders(data.providers);
            EMA.system.printDbgMsg('RPC initialization success');
            $(window).trigger("EMA.ready");
            $dfr.resolve();
        }, function (xhr, status) {
            EMA.system.printDbgMsg('RPC initialization failed', xhr, status);
            $(window).trigger("EMA.failure");
            $dfr.reject();
        });
        return $dfr;
    };


    /**
     * Make custom GET request
     * @param {string} url A url to call
     * @param {*} [payload=''] Payload
     * @return {jQuery.Deferred}
     */
    EMA.GET = function (url, payload) {
        return EMA.system._callBuilder(url, payload, 'GET');
    };

    /**
     * Make custom POST request
     * @param {string} url A url to call
     * @param {*} [payload='']
     * @return {jQuery.Deferred}
     */
    EMA.POST = function (url, payload) {
        return EMA.system._callBuilder(url, JSON.stringify(payload), 'POST', {
            'contentType': 'application/json; charset=UTF-8',
            'processData': false,
        });
    };

    /**
     * Set custom login URL
     * @param {string} url
     */
    EMA.setLoginUrl = function (url) {
        EMA._LOGIN_URL = url;
    };

    /**
     * Set custom logout URL
     * @param {string} url
     */
    EMA.setLogoutUrl = function (url) {
        EMA._LOGOUT_URL = url;
    };

    /**
     * Set custom session check URL
     * @param {string} url
     */
    EMA.setSessionCheckUrl = function (url) {
        EMA._SESSION_URL = url;
    };

    /**
     * Set custom captcha check URL
     * @param {string} url
     */
    EMA.setCaptchaCheckUrl = function (url) {
        EMA._CAPTCHA_URL = url;
    };

    /**
     * All possible RPC methods will be stored in this object
     * @type {{}}
     */
    EMA.Models = {};


    /**
     * RPC methods by access providers
     * @type {{}}
     */
    EMA.ModelsByProviders = {};

    /**
     * Access providers
     * @type {{}}
     */
    EMA.AccessProviders = {};


    EMA.Auth = {};

    /**
     * Holds User data if initiated
     * @type {{}}
     * @private
     */
    EMA.Auth._userData = {};

    EMA.Auth._logInCheckDfr = false;

    /**
     * Set interval id
     * @type {boolean|number}
     * @private
     */
    EMA.Auth._sessionCheckId = false;

    /**
     *
     * @type {boolean}
     * @private
     */
    EMA.Auth._sesionExpired = false;

    /**
     *
     * @type {null}
     * @private
     */
    EMA.Auth._sesionExpiredReason = null;

    EMA.Auth._lastLogoutData = null;

    /**
     * Indicate whatever backend use Extended Auth module
     * @type {boolean}
     */
    EMA.Auth.UseCaptcha = false;

    /**
     * Holds captha dataurl
     * @type {null|string}
     * @private
     */
    EMA.Auth._captchaDataUrl = null;

    /**
     * Perform login
     *
     * NOTE: Promise will be rejected in case of login errors (e.g. invalid password)
     * @param {string} username
     * @param {string} password
     * @param {string} [captcha]
     * @return {jQuery.Deferred}
     * @fires 'EMA.login'
     */
    EMA.Auth.login = function (username, password, captcha) {
        var $dfr = $.Deferred();
        var fp = null;
        var dfp = null;
        if (typeof captcha === 'undefined') {
            captcha = '';
        }

        if (window.hasOwnProperty('ClientJS')) {
            var client = new window.ClientJS();
            fp = client.getFingerprint();
            var dfps = [
                client.getOS(),
                client.getOSVersion(),
                client.getDevice(),
                client.getDeviceVendor(),
                client.getCPU(),
                client.getColorDepth(),
                client.getCurrentResolution(),
                client.getAvailableResolution()
            ];

            dfp = client.getCustomFingerprint.apply(client, dfps);
        }

        EMA.POST(EMA._LOGIN_URL, {'name': username, 'password': password, 'captcha': captcha, 'fp': fp, 'dfp': dfp})
            .then(function (data) {
                var origArgs = arguments;
                EMA.Auth._userData = data;
                EMA.Auth._setChecker();
                EMA.init().always(function () {
                    $(window).trigger('EMA.login', data);
                    $dfr.resolve.apply($, origArgs);
                });

            }, function () {
                $dfr.reject.apply($, arguments);
            });

        return $dfr;
    };

    /**
     * Perform logout
     * @return {jQuery.Deferred}
     * @fires 'EMA.logout'
     */
    EMA.Auth.logout = function () {
        EMA.Auth._removeChecker();
        EMA.Auth._userData = {};
        var $logoutDfr = $.Deferred();

        var result = {
            'expired': EMA.Auth._sesionExpired,
            'expire_reason': EMA.Auth._sesionExpiredReason
        };

        EMA.Auth._lastLogoutData = result;


        if (EMA.Auth._sesionExpired === false) {
            EMA.GET(EMA._LOGOUT_URL).always(function () {
                $logoutDfr.resolve();
            });
        } else {
            EMA.Auth._sesionExpired = false;
            $logoutDfr.resolve();
        }

        var $initDfr = $.Deferred();

        $logoutDfr.done(function () {
            EMA.init().always(function () {
                $(window).trigger('EMA.logout');
                $initDfr.resolve(result);
            });
        });

        return $initDfr;
    };


    /**
     * Check if user logged in
     * @param {boolean} [silent=true] If false - will trigger 'EMA.login' and call EMA.init() in case of user is lodged in
     * @return {jQuery.Deferred}
     * @fires 'EMA.login'
     */
    EMA.Auth.isLoggedIn = function (silent) {
        if (typeof silent !== 'boolean') {
            silent = true;
        }
        var self = this;

        var $dfr = $.Deferred();

        if ($.isEmptyObject(EMA.Auth._userData)) {

            if (typeof this._logInCheckDfr === 'object') {
                this._logInCheckDfr.then(function () {
                    $dfr.resolve.apply($, arguments);
                }, function () {
                    $dfr.reject.apply($, arguments);
                });
            } else {
                this._logInCheckDfr = $dfr;
                EMA.GET(EMA._SESSION_URL).then(function (data) {
                    if (typeof data === 'object') {
                        if ($.isEmptyObject(EMA.Auth._userData)) {
                            EMA.Auth._userData = data;
                        }

                        EMA.Auth._setChecker();
                        if (silent === false) {
                            EMA.init().always(function () {
                                $dfr.resolve(true);
                                $(window).trigger('EMA.login', data);
                            });
                            return;
                        }
                        $dfr.resolve(true);
                    } else {

                        $dfr.resolve(false);
                    }
                }, function () {
                    $dfr.reject.apply($, arguments);
                });

                this._logInCheckDfr.always(function () {
                    self._logInCheckDfr = false;
                });
            }

        } else {
            $dfr.resolve(true);
        }


        return $dfr;
    };

    /**
     * Return user data or empty object if user not logged in
     * @return {{}}
     */
    EMA.Auth.getUserData = function () {
        return EMA.Auth._userData;
    };

    EMA.Auth.getLastLogoutData = function () {
        return EMA.Auth._lastLogoutData;
    };
    /**
     * Check if captcha should be shown to a user
     * @return {jQuery.Deferred}
     */
    EMA.Auth.isCaptchaUsable = function () {
        return $.Deferred(function (dfr) {
            if (EMA.Auth.UseCaptcha === true) {
                EMA.GET(EMA._CAPTCHA_URL).then(function (result) {
                    if (typeof result === 'string' && result.length > 0) {
                        EMA.Auth._captchaDataUrl = result;
                        dfr.resolve(true);
                    } else {
                        dfr.resolve(false);
                    }
                }, dfr.reject);
            } else {
                dfr.resolve(false);
            }
        });
    };

    /**
     * Return captcha image as data url
     *
     * NOTE: isCaptchaUsable() must be called before
     * @return {null|string}
     */
    EMA.Auth.getCaptchaDataUrl = function () {
        return EMA.Auth._captchaDataUrl;
    };

    /**
     * Set interval on session alive checks
     * @private
     */
    EMA.Auth._setChecker = function () {
        if (EMA.Auth._sessionCheckId === false && EMA.sessionChecker) {
            EMA.Auth._sessionCheckId = window.setInterval(function () {
                EMA.GET(EMA._SESSION_URL).then(function (result) {
                    if (result === false) {
                        EMA.Auth.logout();
                    }
                }, function () {
                    EMA.Auth.logout();
                });
            }, EMA._SESSION_INTERVAL * 60 * 1000);
        }
    };

    /**
     * Remove interval of session alive checks
     * @private
     */
    EMA.Auth._removeChecker = function () {
        if (EMA.Auth._sessionCheckId !== false) {
            window.clearInterval(EMA.Auth._sessionCheckId);
            EMA.Auth._sessionCheckId = false;
        }
    };


    EMA.Access = {};

    /**
     * Check whatever current user has access to method o model.
     *
     * Checks are done between all access providers
     * @param {string} accessDescriptor A descriptor with a dot notation, eg `<ModelName>.<MethodName>`
     * @return {boolean}
     */
    EMA.Access.has = function (accessDescriptor) {
        var descriptorObj = this._parseDescriptor(accessDescriptor);
        var methodObj = this._find(descriptorObj);
        if (methodObj) {
            EMA.system.printDbgMsg('Access Rights:', 'Access for `' + accessDescriptor + '` granted ' +
                'by provider ' + methodObj.provider);
        }

        return !!(methodObj);
    };

    /**
     * Lookup for method by accessDescriptor
     * @param {string} accessDescriptor A descriptor with a dot notation, eg `<ModelName>.<MethodName>`
     * @return {(function|false)}
     */
    EMA.Access.getMethod = function (accessDescriptor) {
        var methodObj = this._find(this._parseDescriptor(accessDescriptor));
        if (methodObj) {
            var result = methodObj.method;
        } else {
            throw new Error('Method `' + accessDescriptor + '` not exist or access not allowed');
        }

        return result;
    };

    /**
     * Parses access descriptor string
     * @param {string} accessDescriptor
     * @return {{model: string, method: string}}
     * @private
     */
    EMA.Access._parseDescriptor = function (accessDescriptor) {
        accessDescriptor = accessDescriptor.trim();
        var methodModel = accessDescriptor.split('.');
        if (methodModel.length !== 2) {
            throw new Error('Invalid access descriptor ' + accessDescriptor);
        }
        var regex = /\(.*\)$/;

        var modelName = methodModel[0],
            methodName = methodModel[1];

        methodName = methodName.replace(regex, '');

        return {
            'model': modelName,
            'method': methodName
        }
    };

    /**
     * Looking in for method by descriptorObj
     * @param {{model: string, method: string}} descriptorObj
     * @return {({provider: string, method: function}|boolean)}
     * @private
     */
    EMA.Access._find = function (descriptorObj) {
        function has(modelsObj) {
            return (modelsObj.hasOwnProperty(descriptorObj.model) &&
                modelsObj[descriptorObj.model].hasOwnProperty(descriptorObj.method));
        }


        function makeResult(modelsObj, provider) {
            return {
                'provider': provider,
                'method': modelsObj[descriptorObj.model][descriptorObj.method],
            }
        }

        var result = false;

        if (has(EMA.Models)) {
            result = makeResult(EMA.Models, 'global');
        }

        if (!result) {
            $.each(EMA.ModelsByProviders, function (providerName, modelData) {
                if (has(modelData)) {
                    result = makeResult(modelData, providerName);
                }
                if (result) {
                    /* Brake */
                    return false;
                }
            });
        }

        return result;
    };


    EMA.system = {};


    /**
     * A callback to set additional headers for requests
     * @param {{}} headers
     */
    EMA.system.setRequestHeaderCb = function(headers){};

    /**
     * Builds A RPC request
     * @param {string} modelName
     * @param {string} methodName
     * @param {number} argsNum
     * @private
     * @return {jQuery.Deferred}
     */
    EMA.system._makeRPCRequest = function (modelName, methodName, argsNum) {
        var methodArgs = Array.prototype.slice.call(arguments, 3);

        if (methodArgs.length < argsNum) {
            throw new Error('Rpc call for ' + modelName + '::' + methodName + '() requires at least ' + argsNum +
                ' arguments. ' + methodArgs.length + ' given.');
        }

        return EMA.system._callBuilder(EMA._RPC_BASEPATH + '/' + modelName + '/' + methodName, JSON.stringify(methodArgs),
            'POST', {
                'contentType': 'application/json; charset=UTF-8',
                'processData': false,
                'method': 'POST',
            });
    };

    /**
     * Uploads a file on server
     * @param {string} modelName
     * @param {string} itemId
     * @param {FormData} data
     */
    EMA.system.makeUploadRequest = function (modelName, itemId, data) {
        return EMA.system._callBuilder(EMA._UPLOAD_BATHBATH + '/' + modelName + '/' + itemId, data, 'POST', {
            'processData': false,
            'contentType': false
        });
    };


    /**
     * Make request
     * @param provider
     * @return {jQuery.Deferred}
     * @private
     * @fires 'EMA.providerUpdated'
     */
    EMA.system._makeAccessRequest = function (provider) {
        var url = EMA._ACCESS_REQUEST_URL + '/' + provider;
        var methodArgs = Array.prototype.slice.call(arguments, 1);
        EMA.ModelsByProviders[provider] = {};
        var $dfr = $.Deferred();

        var bindObj = EMA.ModelsByProviders[provider];

        EMA.POST(url, methodArgs).then(function (data) {
            EMA.system._bindModels(data, bindObj);
            $(window).trigger('EMA.providerUpdated', provider);
            $dfr.resolve.apply($, arguments);
        }, function () {
            $dfr.reject.apply($, arguments);
        });
        return $dfr;
    };

    /**
     * Bind providers
     * @param providers
     * @private
     */
    EMA.system._bindProviders = function (providers) {
        EMA.AccessProviders = {};
        $.each(providers, function (index, provider) {
            EMA.AccessProviders[provider] = EMA.system._makeAccessRequest.bind(undefined, provider);
        });
    };

    /**
     * Binds models functions
     * @param {Array} data
     * @param {string|object} [key=Models] name in EMA object or object to bind methods
     * @private
     */
    EMA.system._bindModels = function (data, key) {
        var bindObj = null;
        if (typeof key === 'string') {
            EMA[key] = {};
            bindObj = EMA[key];
        } else if (typeof key === 'object') {
            for (var prop in key) {
                if (key.hasOwnProperty(prop)) {
                    delete key[prop];
                }
            }
            bindObj = key;
        } else {
            EMA.Models = {};
            bindObj = EMA.Models;
        }


        $.each(data, function (modelName, data) {
            bindObj[modelName] = {};
            $.each(data.methods, function (index, methodName) {
                var numOfArgs = data.args[index];
                bindObj[modelName][methodName] = EMA.system._makeRPCRequest.bind(undefined, modelName, methodName, numOfArgs);
            });
        });
    };


    /**
     * Builds A Ajax call
     *
     * @param {string} endPoint - A Url to call
     * @param {*} [payload=""]
     * @param {string} [method='GET']
     * @param {{}} [additionalOptions={}]
     * @private
     * @return {jQuery.Deferred}
     */
    EMA.system._callBuilder = function (endPoint, payload, method, additionalOptions) {
        if (typeof additionalOptions !== 'object') {
            additionalOptions = {};
        }

        if (typeof payload === 'undefined') {
            payload = '';
        }

        if (typeof method === 'undefined') {
            method = 'GET';
        }

        var options = {
            'url': EMA.baseUrl + endPoint,
            'data': payload,
            'method': method,
            'dataType': 'json',
            'headers': {},
        };

        EMA.system.setRequestHeaderCb(options.headers);

        $.extend(options, additionalOptions);

        var csrfCookie = document.cookie.match(/X-EMA-CSRFToken=([\w\-\=\%]+)/);
        if (csrfCookie) {
            options.headers['X-EMA-CSRFToken'] = decodeURIComponent(csrfCookie[1]);
        }

        var $dfr = $.Deferred();

        options['xhr'] = function () {
            var xhr = new window.XMLHttpRequest();
            try {
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        $dfr.notify('up', percentComplete, evt);
                    } else {
                        $dfr.notify('up', 1, evt);
                    }
                }, false);

                xhr.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        $dfr.notify('down', percentComplete, evt);
                    } else {
                        $dfr.notify('down', 1, evt);
                    }
                }, false);
            } catch (err) {
                EMA.system.printDbgMsg(err);
            }


            return xhr;
        };


        $.ajax(options).then(function (data, status, xhr) {
            if (xhr.getResponseHeader('X-Ema-Maintenance')) {
                EMA.Auth._removeChecker();
                $(window).trigger('EMA.maintenance');
                EMA.system.printDbgMsg('Request failed: Application has been set to maintenance mode');
                $dfr.reject(xhr, status);
                return;
            }

            EMA.system.printDbgMsg('Request completed:', endPoint, data);
            $dfr.resolve.apply($, arguments);
        }, function (xhr, status, err) {
            var sessionTimeoutReason = xhr.getResponseHeader('X-EMA-Session-Expire');
            if (status = 419 && sessionTimeoutReason) {
                EMA.system.printDbgMsg('Session expired', sessionTimeoutReason);
                EMA.Auth._sesionExpiredReason = sessionTimeoutReason;
                EMA.Auth._sesionExpired = true;
                EMA.Auth.logout()
            } else {
                EMA.system.printDbgMsg('Request failed:', endPoint, xhr, status, err);
            }

            $dfr.reject.apply($, arguments);
        });


        return $dfr;
    };

    /**
     * Print in console
     * @param {Error|string} exc
     */
    EMA.system.printDbgMsg = function (exc) {
        if (EMA.debug) {
            if (exc instanceof Error) {
                console.log("Exception thrown:\n" + exc.name + " : " + exc.message + "\n");
            } else {
                console.log.apply(window, arguments);
            }
        }
    };


    EMA.tools = {};

    /**
     * Truncates string to given length.
     *
     * @param {string} str
     * @param {int} [len=7]
     * @param {string} [filler="..."]
     * @returns {string}
     */
    EMA.tools.truncateString = function (str, len, filler) {
        if (len === undefined) {
            len = 7;
        }
        if (filler === undefined) {
            filler = "...";
        }
        if (typeof str === "string") {
            if (str.length > len) {
                str = str.substring(0, len);
                str = str + filler;
            }
        }

        return str;
    };

    /**
     * Allow only numeric chars for input element.
     * Add data-isnamonly="true" to any input element on page and call this function.
     */
    EMA.tools.appendIsnamonly = function () {
        $('input[data-isnamonly="true"]').each(function () {
            $(this).keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
    };

    /**
     * Return GET parameter with given name.
     *
     * @param {string} sParam Parameter name
     * @returns {boolean|string}
     */
    EMA.tools.getUrlParameter = function (sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    /**
     * Works like php nl2br function
     *
     * @param {string} str
     * @param {boolean} [is_xhtml]
     * @returns {string}
     */
    EMA.tools.nl2br = function (str, is_xhtml) {
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    };

    /**
     * Checks if string is url encoded data.
     *
     * @param {string} str
     * @returns {boolean}
     */
    EMA.tools.isDataUrl = function (str) {
        if (typeof str == "string") {
            var re = /^\s*data:([a-z]+\/[a-z]+(;[a-z\-]+\=[a-z\-]+)?)?(;base64)?,[a-z0-9\!\$\&\'\,\(\)\*\+\,\;\=\-\.\_\~\:\@\/\?\%\s]*\s*$/i;
            return re.test(str);
        } else {
            return false;
        }
    };

    /**
     * Converts url encoded string to a Blob object
     *
     * @param {string} dataURI
     * @returns {Blob}
     */
    EMA.tools.dataURItoBlob = function (dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {type: mimeString});
    };

    /**
     * Generates random GUID
     * @returns {string} Random GUID
     */
    EMA.tools.guidGenerator = function () {
        var S4 = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    };

    /**
     * Acts like PHPs basename function
     * @param path
     * @param suffix
     * @returns {*|XML|string|void}
     */
    EMA.tools.basename = function (path, suffix) {	// Returns filename component of path
        //
        // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   improved by: Ash Searle (http://hexmen.com/blog/)
        // +   improved by: Lincoln Ramsay
        // +   improved by: djmix

        var b = path.replace(/^.*[\/\\]/g, '');
        if (typeof(suffix) == 'string' && b.substr(b.length - suffix.length) == suffix) {
            b = b.substr(0, b.length - suffix.length);
        }
        return b;
    };
    return EMA;
});

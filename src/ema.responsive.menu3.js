/**
 * jQuery responsive menu plugin
 * @version 0.3.0a
 */


(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        factory(require('jquery'));
    } else {
        factory(root.jQuery);
    }
})(window, function ($) {
    'use strict';


    /**
     *
     * @param {{}} options
     * @param {jQuery} container
     * @constructor
     */
    var ResponsiveMenu = function (options, container) {
        if (typeof options !== 'object') {
            options = {};
        }

        var self = this;
        this.container = container;
        this.options = $.extend(true, {}, $.fn.emaResponsiveMenu.defaults, options);
        this.useBtn = true;

        container.data('instance', this);

        this.$host = this._getHostNode();
        this.$bgNode = null;
        this.$listNode = this.$host.find('.emaResp-container');
        this.$activateNode = this._getActivateNode();
        this.$pageNode = this._getPageContainer();
        this._parseData();
        this._appendList();
        this.isOpened = false;
        this.$activateNode.click(this.toggle.bind(this));

        $(window).resize(function(){
            if (this.isOpened && !this.isMatchWith()) {
                this.close();
            }
        }.bind(this));

        if (this.options.closeOnOutsideClick) {
            this.$host.click(function (e) {
                if (e.target === this) {
                    self.close();
                }
            });
        }


        this.$host.find('a').each(function () {
            $(this).click(function () {
                self.close();
            })
        })
    };

    ResponsiveMenu.prototype.isMatchWith = function () {
        return window.matchMedia("(max-width: " + this.options.maxWidth+"px)").matches;
    };

    ResponsiveMenu.prototype.open = function () {
        var classes = this.options.classes;
        this.$activateNode.addClass(classes.activateNodeOpen);
        this.$bgNode = this._getBackgroundNode();
        this.$bgNode.addClass(classes.hostOpened);
        this.$host.removeClass(classes.hostClosed);
        this.$host.addClass(classes.hostOpened);

        if (this.$pageNode) {
            this.$pageNode.addClass(classes.pageContainer);
        }

        this._preventScroll();

        this.isOpened = true;
    };

    ResponsiveMenu.prototype.close = function () {
        var classes = this.options.classes;

        this.$bgNode.removeClass(classes.hostOpened);
        this.$bgNode.remove();
        this.$bgNode = null;
        this.$activateNode.removeClass(classes.activateNodeOpen);
        this.$host.removeClass(classes.hostOpened);
        this.$host.addClass(classes.hostClosed);
        if (this.$pageNode) {
            this.$pageNode.removeClass(classes.pageContainer);
        }
        this._restoreScroll();
        this.isOpened = false;
    };

    ResponsiveMenu.prototype.toggle = function () {
        if (this.isOpened) {
            this.close();
        } else {
            this.open();
        }
    };

    ResponsiveMenu.prototype._appendList = function () {
        this.$listNode.html(this._generateListHtml(this.options.data));
    };

    ResponsiveMenu.prototype._generateListHtml = function(data, previousId){
        var settings = this.options;
        var html = "<ul>";

        if(previousId !== undefined){
            html += "<li><a href='#back' class='back-bnt' data-unique-id='"+previousId
                +"'><span>" + settings.backButtonText
                +"</span></a></li>"
        }

        $.each(data, function(index, item){
            var target = "";
            if("target" in item){
                target = " target='"+item.target+"' ";
            }

            var appendedClasses = ' ';

            if("class" in item){
                appendedClasses += item['class'];
            }



            if("children" in item){
                appendedClasses += " has-children ";
            }
            if(! ("link" in item)){
                item.link = "#";
            }



            html += "<li><a href='"+item['link']+"' class='"+appendedClasses+"' "+target+"><span>"+item['name']+"</span></a></li>"
        });
        html += "</ul>";
        return html;
    };

    ResponsiveMenu.prototype._getPageContainer = function () {
        var result = null;

        if (this.options.pageContainer) {
            result = this._getJquery(this.options.pageContainer);
        }

        return result;
    };

    ResponsiveMenu.prototype._getBackgroundHtml = function () {
        return '<div class="emaResp-back"></div>';
    };


    ResponsiveMenu.prototype._getBackgroundNode = function () {
        $('body').prepend(this._getBackgroundHtml());
        return $('.emaResp-back');
    };


    ResponsiveMenu.prototype._getHostNode = function () {
        $('body').prepend(this._generateMenuHost());
        return $('.emaResp-host');
    };

    ResponsiveMenu.prototype._parseData = function () {
        var type = typeof this.options.data;
        if (type === 'string' || (type === 'object' && this.options.data instanceof jQuery)) {
            this._generateData();
        }
    };

    ResponsiveMenu.prototype._generateMenuHost = function () {

        var html = '<div class="emaResp-host '+this.options.classes.hostClosed+'">';

        if (this.options.showCloseBtn) {
            if (this.options.closeButtonInHeader) {
                html += '<div class="emaResp-header"><div><button class="emaResp-close--btn "></button></div></div>';
                html += '<div class="emaResp-container"></div>';
            } else {
                html += '<div class="emaResp-container"></div>';
                html += '<div class="emaResp-footer"><div><button class="emaResp-close--btn "></button></div></div>';
            }
        } else {
            html += '<div class="emaResp-container"></div>';
        }

        html += '</div>';

        return html;
    };

    ResponsiveMenu.prototype._generateBtnHtml = function () {
        var imgOptions = this.options.openImg;
        return '<div class="emaRespIcon--wrapper"><img width="' + imgOptions.width + '" height="' + imgOptions.height + '"></div>';
    };

    ResponsiveMenu.prototype._generateData = function () {
        var selector = this.options.data;
        if(typeof selector === "string"){
            var $element = $(selector);
        } else {
            var $element = selector;
        }

        function parseHtml(node) {
            var currentNode = [];
            node.find("li").each(function () {
                var $this = $(this);
                var current_object = {
                    'class': ""
                };

                current_object['name'] = $this.children("a").text();
                current_object['link'] = $this.children("a").attr("href");
                var target = $this.find("a").attr("target");
                var classes = $this.find("a").attr("class");

                if(typeof target === "string"){
                    current_object['target'] = target;
                }

                if(typeof classes === "string"){
                    current_object['class'] = classes;
                }

                var $childrenNode = $this.children("ul");

                if($childrenNode.length > 0){
                    var children = parseHtml($childrenNode);
                    if(children.length > 0){
                        current_object['children'] = children;
                    }
                }

                currentNode.push(current_object);

            });
            return currentNode;
        }

        this.options.data =  parseHtml($element);
    };

    /**
     *
     * @returns {jQuery}
     * @private
     */
    ResponsiveMenu.prototype._getActivateNode = function () {
        if (this.options.openBtn) {
            return this._getJquery(this.options.openBtn)
        } else {
            this.container.prepend(this._generateBtnHtml());
            this.useBtn = false;
            return this.container.find('.emaRespIcon--wrapper');
        }
    };

    ResponsiveMenu.prototype._preventScroll = function() {
        if (this.options.blockPageScroll) {
            $('html, body').css({
                overflow: 'hidden',
                height: '100%'
            });
        }

    };

    ResponsiveMenu.prototype._restoreScroll = function() {
        if (this.options.blockPageScroll) {
            $('html, body').css({
                overflow: 'auto',
                height: 'auto'
            });
        }
    };

    /**
     *
     * @param {string|jQuery} selector
     * @returns {jQuery|false}
     * @private
     */
    ResponsiveMenu.prototype._getJquery = function (selector) {
        var type = typeof selector;
        if (type === 'string') {
            return $(selector);
        } else if (type === 'object' && selector instanceof jQuery) {
            return selector;
        } else {
            return false;
        }
    };






    $.fn.emaResponsiveMenu = function (options) {
        new ResponsiveMenu(options, this);
        return this;
    };

    $.fn.emaResponsiveMenu.defaults = {
        /* Data source */
        'data': [],
        'maxWidth': 900,
        'blockPageScroll': false,
        'showCloseBtn': false,
        'closeButtonInHeader': true,

        'backButtonText': 'Back',

        'closeOnOutsideClick': true,

        /* Selector or instance of main page */
        'pageContainer': null,

        /* Class or selector for open button */
        'openBtn': null,
        /* OR */
        'openImg': {
            'width': 40,
            'height': 40,
            'normal': null,
            'active': null,
            'sticky': null,
        },
        'delays': {

        },
        'classes': {
            'activateNodeOpen': 'open',
            'hostOpened': 'open',
            'hostClosed': 'closed',
            'pageContainer': 'respMenu-open',
        }

    };
});



/**
 * EMA engine RPC library
 * @author Illia Sazhko <babai.linux@gmail.com>
 * @version 1.0.0
 * @licence MIT
 * @module emaPopup
 * @requires jquery
 */
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        module.exports = factory(require('jquery'));
    } else {
        if (!('EMA' in root)) {
            root['EMA'] = {};
        }
        root.EMA.PopUpConstructor = factory(root.jQuery);
    }
})(window, function ($) {
    'use strict';

    /**
     * Constructs popup
     *
     * Use static methods instead
     * @param {{}} [options] Popup options
     * @param {number} [options.timeout=5] Time in seconds whenever popup should disappear
     * @param {string} [options.type=error] Type of popup, can be: `error`, `info` or `success`
     * @param {number} [options.z-index=3000] Popup Z-index
     * @param {number} [options.baseOffset=300] Offset in px from the top of the window
     * @param {number} [options.animationDuration=500] Duration in ms for popup animation
     * @constructor
     * @alias module:emaPopup
     */
    var PopUpConstructor = function (options) {
        if(typeof options !== 'object') {
            options = {};
        }

        $.extend(this, this.defaults, options);
        this.guid = null;
        this.$hostNode = null;
        this.$popupNode = null;
    };


    PopUpConstructor.prototype.defaults = {
        'message': '',
        "timeout": 5,
        'type': 'error', /* Types: 'error', 'info', 'success' */
        'z-index': 3000,
        'baseOffset': 300,
        '_host': 'popup-host',
        'animationDuration': 500
    };

    /**
     * Show popup
     * @param {string} message Massage to show in popup. Can have simple html formatting.
     */
    PopUpConstructor.prototype.open = function (message) {
        this.message = message;
        this._checkHost();

        this.guid = this._getGuid();

        this.$hostNode.prepend(this._getHtml(message));
        this.$popupNode = $('#' + this.guid);
        this.$popupNode.animate({
            opacity: 1,
            top: 0
        }, this.animationDuration);

        setTimeout(function () {
            this._destroy();
        }.bind(this), this.timeout * 1000);
    };

    /**
     * Destroys a popup
     * @private
     */
    PopUpConstructor.prototype._destroy = function () {
        this.$popupNode.animate({opacity: 0}, {
            'duration': this.animationDuration,
            'complete': function () {
                this.$popupNode.remove();
                this.$popupNode = null;
            }.bind(this)
        })
    };

    /**
     * Checks if host node exist or create it
     * @private
     */
    PopUpConstructor.prototype._checkHost = function () {
        this.$hostNode = $('#'+this._host);
        if (this.$hostNode.length < 1) {
            $('body').prepend('<div id="' + this._host + '"></div>');
            this.$hostNode = $('#'+this._host);
        }
    };

    /**
     * Generates offset
     * @return {number}
     * @private
     */
    PopUpConstructor.prototype._getTopOffset = function () {
        return -Math.abs(this.$hostNode.find('.popup-outer').length * this.baseOffset);
    };

    /**
     * Generates HTML for popup
     * @param {string} message
     * @return {string}
     * @private
     */
    PopUpConstructor.prototype._getHtml = function (message) {
        // var offset = this._getTopOffset();
        return '<div class="popup-outer" id="' + this.guid + '" style="opacity: 0; top: -300px">' +
            '<div class="popup-inner ' + this.type + '">' + message + '</div></div>';
    };

    /**
     * Generates guid
     * @return {string}
     * @private
     */
    PopUpConstructor.prototype._getGuid = function () {
        var S4 = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    };

    /**
     * Create and show error popup
     * @param {string} message
     * @param {number} [time=5]
     */
    PopUpConstructor.showError = function (message, time) {
        if (!time) {
            time = 5;
        }
        new PopUpConstructor({"timeout": time, 'type': 'error'}).open(message);
    };

    /**
     * Create and show info popup
     * @param {string} message
     * @param {number} [time=5]
     */
    PopUpConstructor.showInfo = function (message, time) {
        if (!time) {
            time = 5;
        }
        new PopUpConstructor({"timeout": time, 'type': 'info'}).open(message);
    };

    /**
     * Create and show success popup
     * @param {string} message
     * @param {number} [time=5]
     */
    PopUpConstructor.showSuccess = function (message, time) {
        if (!time) {
            time = 5;
        }
        new PopUpConstructor({"timeout": time, 'type': 'success'}).open(message);
    };

    return PopUpConstructor;
});

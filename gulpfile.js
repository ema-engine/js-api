
var gulp = require('gulp'),
    jsdoc = require('gulp-jsdoc3'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    smp = require('gulp-sourcemaps');


gulp.task('js', function () {
    return gulp.src('./src/*.js')
        .pipe(gulp.dest('build/'))
        .pipe(rename(function (path) {
            path.basename += '.min'
        }))
        .pipe(smp.init())
        .pipe(uglify())
        .pipe(smp.write('./'))
        .pipe(gulp.dest('build/'));
});

gulp.task('doc', function (cb) {
    gulp.src(['./src/**/*.js'], {read: false})
        .pipe(jsdoc({
            "tags": {
                "allowUnknownTags": true
            },
            "opts": {
                "destination": "./docs",
                "private": false
            },
            "plugins": [
                "plugins/markdown"
            ],
            "templates": {
                "cleverLinks": false,
                "monospaceLinks": false,
                "default": {
                    "outputSourceFiles": true
                },
                "path": "ink-docstrap",
                "theme": "cerulean",
                "navType": "vertical",
                "linenums": true,
                "dateFormat": "MMMM Do YYYY, h:mm:ss a"
            }
        }, cb));
});

gulp.task('default', ['js', 'doc']);